import './App.css';
import React from 'react';

const schoolTable = ["Pies zjadł mi zeszyt!","Zapomniałem","Kosmici zaanektowalimój pokój!","To było jakieś zadanie?","Strasznie źle się czułem...","Tata dał mi bojowe zadanie! Nie mogłem odmówić...","Zapomniałem. Mogę zrobic je jutro ?"]

const workTable = ['Szefie, samochód mi się zepsół!',"Jestem chory. Idę do lekarza.",'Złamałem nogę.','Przepraszam, nie dam rady być dziś w pracy. Zaczęłam rodzić.',];

const relationshipTable = ['Kochanie, to nie tak jak myślisz.', 'Nie mogę być na kolacji z teściami. O 20 mam przewidziany pożar w domu.', 'Wcale nie gram w gry dla przyjemności... to część mojej pracy.'];

const partyTable = ["Przepraszam, nic nie pamiętam.", 'Niestety, muszę nadrobić zaległości w pracy.', 'Będę miał/a czas dopiero w przyszłym miesiącu.','Muszę zostać z dzieckiem.','Nie mogę. Stara mi będzie marudzić.'];

const list = [
  {
    "name": "szkoła",
    "data": schoolTable
  },
  {
    "name": "praca",
    "data": workTable
  },
  {
    "name": "relacje",
    "data": relationshipTable
  },
  {
    "name": "party",
    "data": partyTable
  }
];

class Generator extends React.Component {
  state = {
    txt: '',
    list: {}
  }

  constructor(props) {
     super(props);
     this.state.list = props.list;
  }

  handleClick(e, key) {
    const table = this.state.list[key].data;
    this.setState({
      txt: table[Math.floor(Math.random() * table.length)]
    })
  }

  showMessage = () => {
    return <h1>TXT: { this.state.txt }</h1>
  }

  render() {
    console.log('Rendering...');
    return (
        <>
          <h1>Generator wymówek</h1>

          {this.state.list.map((table, key) => {
            return <button onClick={(e) => {
              this.handleClick(e, key);
            }} key={ key }>{ table.name }</button>;
          })}

          { this.showMessage() }
        </>
    )}
}

function App() {
  return (
    <div className="App">
      <header className="App-header">
      </header>
      <Generator list={ list } />
    </div>
  );
}

export default App;
